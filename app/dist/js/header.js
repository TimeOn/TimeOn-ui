jQuery(document).ready(function() {
$('#placeForHeader').html('<header class="main-header">\
        <a href="../index2.html" class="logo">\
            <span class="logo-mini"><b>T</b>ON</span>\
            <span class="logo-lg"><b>TimeOn</b></span>\
        </a>\
        <nav class="navbar navbar-static-top">\
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">\
                <span class="sr-only">Toggle navigation</span>\
                <span class="icon-bar"></span>\
                <span class="icon-bar"></span>\
                <span class="icon-bar"></span>\
            </a>\
            <div class="navbar-custom-menu">\
                <ul class="nav navbar-nav">\
                    <li >\
                        <div >\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" style="margin-top:7px">\
                                    Добавить\
                                    <span class="fa fa-caret-down"></span></button>\
                                <ul class="dropdown-menu" role="menu">\
                                    <li><a href="event.html">Cобытие</a></li>\
                                    <!--<li><a href="#">Цель</a></li>-->\
                                </ul>\
                            </div>\
                        </div>\
                    </li>\
                    <li style="display: none;" class="dropdown notifications-menu">\
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">\
                            <i class="fa fa-bell-o"></i>\
                            <span class="label label-warning">10</span>\
                        </a>\
                        <ul class="dropdown-menu">\
                            <li class="header">У вас 10 уведомлений</li>\
                            <li>\
                                <ul class="menu">\
                                    <li>\
                                        <a href="#">\
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today\
                                        </a>\
                                    </li>\
                                    <li>\
                                        <a href="#">\
                                            <i class="fa fa-warning text-yellow"></i> Very long description here that\
                                            may not fit into the\
                                            page and may cause design problems\
                                        </a>\
                                    </li>\
                                    <li>\
                                        <a href="#">\
                                            <i class="fa fa-users text-red"></i> 5 new members joined\
                                        </a>\
                                    </li>\
                                    <li>\
                                        <a href="#">\
                                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made\
                                        </a>\
                                    </li>\
                                    <li>\
                                        <a href="#">\
                                            <i class="fa fa-user text-red"></i> You changed your username\
                                        </a>\
                                    </li>\
                                </ul>\
                            </li>\
                            <li class="footer"><a href="#">View all</a></li>\
                        </ul>\
                    </li>\
                    <li  id="liForName" class="dropdown user user-menu">\
                    </li>\
                        </ul>\
                    </li>\
                </ul>\
            </div>\
        </nav>\
    </header>\
    <aside class="main-sidebar">\
        <section class="sidebar">\
            <ul class="sidebar-menu" data-widget="tree">\
                <li >\
                    <a href="calendar.html">\
                        <i class="fa fa-calendar"></i> <span>Календарь</span>\
                    </a>\
                </li>\
                <li>\
                    <a href="myGroups.html">\
                        <i class="fa fa-pie-chart"></i> <span>Группы</span>\
                    </a>\
                </li>\
                <li>\
                    <a href="search.html">\
                        <i class="fa fa-laptop"></i> <span>Поиск</span>\
                    </a>\
                </li>\
                <li>\
                    <a href="meetings.html">\
                        <i class="fa fa-pie-chart"></i> <span>Встречи</span>\
                    </a>\
                </li>\
            </ul>\
        </section>\
    </aside>');    
});
