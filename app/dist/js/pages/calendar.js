/*Подгрузка информации о пользователе в хеадер*/
var currentCalendars = [];
var timeonCalendar
var viewStart;
var viewEnd;

var myId;
console.log("myId до изменения"+myId)
$.ajax({
          type: 'GET',
          beforeSend: function(request){
            request.setRequestHeader("Auth-Token", Cookies.get('Auth-Token'));
            },
          url: '/api/me/id',
          success: function(data){
            myId=data;
            console.log("myId после изменения"+myId)
          }
});


jQuery(document).ready(function($) {
     if (Cookies.get('Auth-Token' ) !== undefined) {
         $.ajax({
          type: 'GET',
          beforeSend: function(request){
            request.setRequestHeader("Auth-Token", Cookies.get('Auth-Token'));
            },
          url: '/api/getName',
          success: function(data){
            console.log("myId для профиля"+myId)
            $("#liForName").html('<a href="#" class="dropdown-toggle" data-toggle="dropdown">'+ 
            '<span class="hidden-xs">'+ data  +'</span>'+"</a>" + ' <ul class="dropdown-menu" role="menu">' + 
            '<li >'+
            '<a href="profile.html?id='+myId+'" align="center">Профиль</a>'+
            '</li>' +
            '<li >'+
            '<a href="login.html" align="center" onclick="forExitBtn();" >Выйти</a>'+
            '</li>');
            $('#clickDialog').removeClass("hidden");
          }
        }); 
         $.ajax({
            type:'GET',
            beforeSend: function(request){
              request.setRequestHeader("Auth-Token", Cookies.get('Auth-Token'));
            },
            url: '/api/my/calendars',
            statusCode: {
                200: function(response){
                  var counter = 0 ;

                  $('#inputCalendar').empty();
                  response.forEach(function(item, i, arr){   
                  if(counter == 0){
                    timeonCalendar = item.id;
                    }         
                  var element = $('<option value="'+item.id+'">' + item.name+'</option>');
                  $('#inputCalendar').append(element);
                  currentCalendars.push(item.id);
                  counter++;
                  })
                  loadEvents(viewStart, viewEnd)
                  
                }
              }          
           });
         } 
    else{
      $("#liForName").html("<a href='examples/login.html'>Войти</a>")
    }

 /* Тест для url params
  var urlParams = new URLSearchParams(window.location.search);
  console.log(urlParams.get('id'))*/
   /* запрос на евенты текущего месяца*/

   loadEvents("2018-03-12T15:49:45.057Z", "2018-05-14T15:49:45.057Z" )
    
   $('#clickDialog').removeClass("hidden");
})


$(function () {
  /*Глобальные, для этой функции, переменные, я колхоз*/
  var currentClickEvent;
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    /*Здесь инициализация календаря и событий, связанных с ним*/
    $('#calendar').fullCalendar({
      language: 'ru',
      height: 'auto',
      eventDrop: function(event, delta, revertFunc) {
        if (!confirm("Вы уверены?")) {
          revertFunc();
        } else {
          var newStart = event.start;
          var newEnd = event.end;
          editEvent(event.id, event.newAddress, newStart, newEnd, event.description, event.title, event.tag )
        }
      },
      eventResize: function(event, delta, revertFunc) { 
        if (!confirm("Вы уверены?")) {
          revertFunc();
        } else{        
          var newEnd = event.end;
          editEvent(event.id, event.newAddress, event.start, newEnd, event.description, event.title, event.tag );
        }
      },
    
     /* Здесь будет подгрузка событий */
      viewRender: function(view,element){
        $('#calendar').fullCalendar( 'removeEvents', function(event) {
          return true;
        });
        loadEvents(fromMomentToLocalDateToGet(view.start), fromMomentToLocalDateToGet(view.end));
        viewStart =  fromMomentToLocalDateToGet(view.start);
        viewEnd =  fromMomentToLocalDateToGet(view.end);
      },
      /*events: function (start, end, timezone, callback) {
        loadEvents(fromMomentToLocalDate(start), fromMomentToLocalDate(end))
      },*/
      dayClick: function (date, jsEvent, view){
        $("#dayClickBtn").removeClass("hidden");
        $("#eventClickBtn").addClass("hidden");
        $('#eventForm')[0].reset();
        var startDate = date.format('DD-MM-YYYY HH:mm');
        var endDate = moment(date).add(1, 'h').format('DD-MM-YYYY HH:mm');
         /*Инициализация datetimepicker для поля "Начало и завершение" формы добавления события*/
        $("#evenRangeTime").data('daterangepicker').setStartDate(startDate);
        $("#evenRangeTime").data('daterangepicker').setEndDate(endDate);
          //$('#evenRangeTime').val(startDate + ' - ' +   endDate)

         /*ДИАЛОГ JQUERY UI ПО КЛИКУ НА ДЕНЬ*/
          $('#clickDialog').dialog('option', 'title', 'Создать событие');        
          $("#clickDialog").dialog("option", "position", {
             my: "left+5 bottom-5",
             of: jsEvent // this refers to the cliked element
           }).dialog("open");
      },

       eventClick: function(calEvent, jsEvent, view) {
        currentClickEvent = calEvent;
        $("#dayClickBtn").addClass("hidden");
        $("#eventClickBtn").removeClass("hidden");
        $('#eventForm')[0].reset();
        $('#eventTitle').val(calEvent.title)
        $("#evenRangeTime").data('daterangepicker').setStartDate(calEvent.start.format('DD-MM-YYYY HH:mm') );
        $("#evenRangeTime").data('daterangepicker').setEndDate(calEvent.end.format('DD-MM-YYYY HH:mm'));
        $('#evenRangeTime').val(calEvent.start.format('DD-MM-YYYY HH:mm') + ' - ' +   calEvent.end.format('DD-MM-YYYY HH:mm'))
        $('#clickDialog').dialog('option', 'title', 'Редактировать');
       /* Открывается диалог, задается позиция относительно точки клика*/
        $("#clickDialog").dialog("option", "position", {
           my: "left+5 bottom-5",
           of: jsEvent // this refers to the cliked element
         }).dialog("open");
      },
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'Сегодня',
        month: 'Месяц',
        week : 'Неделя',
        day  : 'День'
      },
      editable  : true,
      droppable : true, 
    })    

    /*  Обработчик кнопки добавления события*/
    jQuery("#addEventBtn").click(function(e){
      e.preventDefault();
     /* Парсим данные из инпута для формирования дат начала и конца*/
      var start = moment($('#evenRangeTime').val().split(' - ')[0], "DD-MM-YYYY HH:mm");
      var end = moment($('#evenRangeTime').val().split(' - ')[1], "DD-MM-YYYY HH:mm");
      var localStart;
      var localEnd;
      var tag = Number($("#inputTag").val());
      /*Цикл по всем датам в заданном промежутке*/
      for (var m = moment(start); m.isBefore(end); m.add(1, 'days')) {
      /*  Наверняка промежуток времени можно задать адекватнее. Но я устал*/
        localStart = m.clone();
        localStart.set({
           'hour' : $('#evenRangeTime').val().split(' ')[1].split(':')[0],
           'minute'  :  $('#evenRangeTime').val().split(' ')[1].split(':')[1]
        }); 
        localEnd = m.clone();
        localEnd.set({
           'hour' : $('#evenRangeTime').val().split(' ')[4].split(':')[0],
           'minute'  :  $('#evenRangeTime').val().split(' ')[4].split(':')[1]
        });
          addCalendarEvent(
          localStart,
          localEnd,
          $('#eventTitle').val(),
          )
      }
      
      $("#clickDialog" ).dialog('close');
    })
    

   /* Добавление одинарного события*/
    function addCalendarEvent(start, end, title) {
      /* Отправка в БД*/
        
   /*     var newEvent={
        "name": title,
        "address":  null,
        "dateTimeBegin": fromMomentToLocalDate(start),
        "dateTimeEnd": fromMomentToLocalDate(end),
        "timeZone":Intl.DateTimeFormat().resolvedOptions().timeZone,
        "recurrence": null,
        "description": null,
        "calendars": timeonCalendar
    }        */
        var newEvent={
       "address": null,
        "calendars": [
           timeonCalendar
          ],
        "dateTimeBegin":  fromMomentToLocalDate(start),
        "dateTimeEnd": fromMomentToLocalDate(end),
        "description": null,
        "name": title,
        "recurrence": null,
        "timeZone": Intl.DateTimeFormat().resolvedOptions().timeZone
    }
    $.ajax({
      type:'POST',
      beforeSend: function(request){
        request.setRequestHeader("Auth-Token", Cookies.get('Auth-Token'));
      },
  
      url: '/api/event?timeZone='+Intl.DateTimeFormat().resolvedOptions().timeZone,
      data: JSON.stringify(newEvent),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
    statusCode: {
        200: function(response){

        }
    }    
  });
  var eventObject = {
    id: "hardcode",
    start: moment(start),
    end: moment(end),
    title: title,
    color: getColor(null)
  };
  $('#calendar').fullCalendar('renderEvent', eventObject, true);
}

  /*  Инициализация формы для клика по дню или событию*/
    $( function() {
      $( "#clickDialog" ).dialog({
        appendTo: ".control-sidebar, .navbar ",
        autoOpen: false,
        resizable: false,
        show: {
        effect: "fade",
        duration: 100
        },
        hide: {
          effect: "fade",
          duration: 100
        },
        width: "30%",
       // maxWidth: "600px"
      });
    });

  /*    Инициализация поля выбора времени*/
  $('#evenRangeTime').daterangepicker({
    timePicker: true, 
    timePickerIncrement: 5, 
    "timePicker24Hour" : true, 
    locale: { 
      format: 'DD-MM-YYYY H:mm',             
      applyLabel: 'Применить',
      cancelLabel: 'Отмена'
    }
  })

jQuery("#deleteEventBtn").click(function(e){
  e.preventDefault();
  $('#calendar').fullCalendar('removeEvents', currentClickEvent.id);
  $("#clickDialog" ).dialog('close');
     $.ajax({
      type: 'DELETE',
      headers: {
        "Auth-Token" :  Cookies.get('Auth-Token')
        },
      url: '/api/events/' + currentClickEvent.id,
      statusCode: { 
        200: function(response){
        }
      }
    }); 
  });

 jQuery("#editEventBtn").click(function(e){
  e.preventDefault();
  var start = moment($('#evenRangeTime').val().split(' - ')[0], "DD-MM-YYYY HH:mm");
  var end = moment($('#evenRangeTime').val().split(' - ')[1], "DD-MM-YYYY HH:mm");
  var tag = Number($("#inputTag").val());
  var name = $('#eventTitle').val();
  var color = getColor(Number($("#inputTag").val())); 

/* TO DO Вынести обновку в отдельную функцию*/
    var dataToPut = {
      "address": "ahaha",
      "dateTimeBegin":  moment(start).format('YYYY-MM-DDTHH:mm:ss') + '.057Z',
      "dateTimeEnd":  moment(end).format('YYYY-MM-DDTHH:mm:ss') + '.057Z',
      "description": "string",
      "name": name,
      "tag": tag,
      "type": "individual_eventDTO"
    }
     $.ajax({
      type: 'PUT',
      beforeSend: function(request){
        request.setRequestHeader("Auth-Token", Cookies.get('Auth-Token'));
        },
      url: '/api/events/' + currentClickEvent.id,
      data: JSON.stringify(dataToPut),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      statusCode: { 
        200: function(response){
        },

        403: function(response){
        }                   
      }
    });
    currentClickEvent.title = $('#eventTitle').val();
    currentClickEvent.start = moment($('#evenRangeTime').val().split(' - ')[0], "DD-MM-YYYY HH:mm").toDate();
    currentClickEvent.end = moment($('#evenRangeTime').val().split(' - ')[1], "DD-MM-YYYY HH:mm").toDate();
    currentClickEvent.color = getColor(tag);
    $('#calendar').fullCalendar('updateEvent', currentClickEvent);  
    $("#clickDialog" ).dialog('close');
  })

/*Конец function*/
})

  /* Возварщает цвет по тегу*/
function getColor(ext_id) {
  if (ext_id == null)
    return'#006400';
  if (ext_id.length == 26 || ext_id.length == 43)
    return  '#8B0000';
  if (ext_id.length > 44)
    return '#4169E1';
}
function getTag(color) {
  switch (color) {
    case '#4169E1':
      return 1;
      break;
    case '#FF4500':
      return 2 ;  
      break;
    case '#006400':
      return 3;
      break;
    case '#8B0000':
      return 4;
      break;
    default:
      console.log('error with tag');
  }

}

function loadEvents(fromDate, toDate){

  $.ajax({
    type: 'GET',
    headers: {
      "Auth-Token" :  Cookies.get('Auth-Token')
      },
    data: {
      "fromDate" :  fromDate,
      "toDate" :  toDate,
      "timeZone": "Europe/Moscow",
      "calendarIds": currentCalendars.toString()
    },
    url: '/api/me/events',
    statusCode: { 
      200: function(response){
        response.forEach(function(item, i, arr){
          /*TO DO: разобраться, почему даты на бэке даты на месяц вперед, сейчас костыли*/
          var startDate = item.dateTimeBegin;
          var endDate = item.dateTimeEnd;
          var eventObject = {
            id: item.id,
            start: moment(startDate),
            end: moment(endDate),
            title: item.name,
            color: getColor(item.ext_id)
          };
          $('#calendar').fullCalendar('renderEvent', eventObject, true);
        })
      }
    }
  }); 

}

function editEvent(id, newAddress, newDateTimeBegin, newDateTimeEnd, newDescription, newName, newTag ){
      var dataToPut = {
      "address": newAddress,
      "dateTimeBegin":  fromMomentToLocalDate(newDateTimeBegin),
      "dateTimeEnd":  fromMomentToLocalDate(newDateTimeEnd),
      "description": newDescription,
      "name": newName,
      "tag": newTag,
      "type": "individual_eventDTO"
    }
     $.ajax({
      type: 'PUT',
      beforeSend: function(request){
        request.setRequestHeader("Auth-Token", Cookies.get('Auth-Token'));
        },
      url: '/api/events/' + id,
      data: JSON.stringify(dataToPut),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      statusCode: { 
        200: function(response){
        },

        403: function(response){
        }                   
      }
    });
}

/*TO DO разобараться почему не работает без .057z*/
function fromMomentToLocalDate(date){
  return moment(date).format('YYYY-MM-DDTHH:mm:ss') + '.057Z'
}

function fromMomentToLocalDateToGet(date){
  return moment(date).format('YYYY-MM-DDTHH:mm:ss') + '.799Z'
}

function forExitBtn(){
  Cookies.remove('Auth-Token');
}

jQuery("#submitCalendarsBtn").click(function(e){
  currentCalendars = $('#inputCalendar').val();
  console.log(currentCalendars);
  console.log(timeonCalendar);
   $('#calendar').fullCalendar( 'removeEvents', function(event) {
      return true;
    });
  loadEvents(viewStart, viewEnd)
 
})


